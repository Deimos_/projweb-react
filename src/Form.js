import React, { Component } from 'react';
import $ from "jquery";
import './index.css';
import './Templates/Form.css';

export default class Form extends Component {

    constructor (props) {
        super();
        this.state = {
            avatar: "Ninguno",
            nombres: "",
            apellidos: "",
            direccion: "",
            ciudad: "",
            telefono: "",
            email: "",
            fenac: "",
            lugnac: "",
            genero: "",
            nacionalidad: "",
            estadocivil: "",
            permisoconducir: "",
            pagweb: "",
            idiomas: "",
            resumen: ""
        }

        this.handleChange = this.handleChange.bind(this)
    }

    getState = () => {
        return JSON.stringify(this.state)
    }

    componentDidMount() {
        var newState = new Object();
        $("#formulario").children("input").each(
            () => {
                var attr = this.id;
                newState[attr] = this.value;
            })

        this.setState(newState)

    }

    handleChange(e) {
        let prevState = this.state
        prevState[e.target.id] = e.target.value
        if(e.target.name == "template"){
            prevState[e.target.name] = e.target.value
        }
        this.setState(prevState)
        this.props.fState(this.state)
    }

    build(e) {
        e.preventDefault()
        alert("To Build")
    }

    render() {
        return (
            <div className="split left" onChange={this.handleChange}>
                <form itemID="formulario" className="formulario">
                    <h2 className="formulario__titulo">Plantillas</h2>
                    <div className="contenedor-plantillas">
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="auckland"/>
                                <img src={require("./Templates/auckland/preview.png")}/>
                                <p>Auckland</p>
                            </label>
                        </div>
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="berkeley"/>
                                <img src={require("./Templates/berkeley/preview.png")}/>
                                <p>Berkeley</p>
                            </label>
                        </div>
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="cambridge"/>
                                <img src={require("./Templates/cambridge/preview.png")}/>
                                <p>Cambridge</p>
                            </label>
                        </div>
                    </div>

                    <div className="contenedor-plantillas">
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="otago"/>
                                <img src={require("./Templates/otago/preview.png")}/>
                                <p>Otago</p>
                            </label>
                        </div>
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="oxford"/>
                                <img src={require("./Templates/oxford/preview.png")}/>
                                <p>Oxford</p>
                            </label>
                        </div>
                        <div className="plantilla">
                            <label>
                                <input className="radio" name="template" type="radio" value="princeton"/>
                                <img src={require("./Templates/princeton/preview.png")}/>
                                <p>Princeton</p>
                            </label>
                        </div>
                    </div>

                    <h2 className="formulario__titulo">Datos Personales</h2>
                    <input className="formulario__input" id="nombres" type="text" /> <br />
                    <label className="formulario__label" htmlFor="nombres">Nombre(s)</label>

                    <input className="formulario__input" id="apellidos" type="text" /> <br />
                    <label className="formulario__label" htmlFor="apellidos">Apellidos</label>

                    <input className="formulario__input" id="direccion" type="text" /> <br />
                    <label className="formulario__label" htmlFor="direccion">Direccion</label>

                    <input className="formulario__input" id="ciudad" type="text" /> <br />
                    <label className="formulario__label" htmlFor="ciudad">Ciudad</label>

                    <input className="formulario__input" id="telefono" type="text" /> <br />
                    <label className="formulario__label" htmlFor="telefono">Telefono</label>

                    <input className="formulario__input" id="email" type="text" /> <br />
                    <label className="formulario__label" htmlFor="email">Email</label>

                    <input className="formulario__input" id="fenac" type="date" /> <br />
                    <label className="formulario__label" htmlFor="fenac">Fecha de Nacimiento</label>

                    <input className="formulario__input" id="lugnac" type="text" /> <br />
                    <label className="formulario__label" htmlFor="lugnac">Lugar de Nacimiento</label>

                    <input className="formulario__input" id="genero" type="text" /> <br />
                    <label className="formulario__label" htmlFor="genero">Genero</label>

                    <input className="formulario__input" id="nacionalidad" type="text" /> <br />
                    <label className="formulario__label" htmlFor="nacionalidad">Nacionalidad</label>

                    <input className="formulario__input" id="estadocivil" type="text" /> <br />
                    <label className="formulario__label" htmlFor="estadocivil">Estado Civil</label>

                    <input className="formulario__input" id="permisoconducir" type="text" /> <br />
                    <label className="formulario__label" htmlFor="permisoconducir">Permiso de Conducir</label>

                    <input className="formulario__input" id="pagweb" type="text" /> <br />
                    <label className="formulario__label" htmlFor="pagweb">Pagina Web</label>

                    <input className="formulario__input" id="idiomas" type="text" /> <br />
                    <label className="formulario__label" htmlFor="idiomas">Idiomas</label>

                    <input className="formulario__input" id="resumen" type="text" /> <br />
                    <label className="formulario__label" htmlFor="resumen">Resumen</label>

                    <input className="formulario__submit" value="Construir" type="text" onClick={this.build}/> <br />
                </form>
            </div>
        )
    }
}
