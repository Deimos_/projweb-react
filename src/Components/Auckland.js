import React, { Component } from 'react'
import '../Templates/auckland/css.css'

export default class Auckland extends Component {
    constructor(props) {
        super()
        this.state = {}
        console.log("AUCKLAND",this.state)
    }
    render() {
        return ( 
            <div>

                <div className="avatar"/>

                <div className="sidebar--bg"/>

                <div className="section--name">
                    <h1>{this.props.data.nombres.split(" ",1)+" "} {this.props.data.apellidos.split(" ",1)} </h1>
                </div>

                <aside className="sidebar">

                    <div className="sidebar--data">
                        <h2 className="sidebar--title">
                            Personal
                        </h2>

                        <ul className="sidebar--list">
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Nombre</span>
                                <br/>
                                <span>{this.props.data.nombres}</span>
                                <span>{this.props.data.apellidos}</span>
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Direccion</span>
                                <br/>
                                {this.props.data.direccion + " "}
                                <b>{this.props.data.ciudad} </b>
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Número de teléfono</span>
                                <br/>
                                {this.props.data.telefono}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Correo electrónico</span>
                                <br/>
                                {this.props.data.email}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Fecha de Nacimiento</span>
                                <br/>
                                {this.props.data.fenac}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Lugar de nacimiento</span>
                                <br/>
                                {this.props.data.lugnac}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Género</span>
                                <br/>
                                {this.props.data.genero}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Nacionalidad</span>
                                <br/>
                                {this.props.data.nacionalidad}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Estado Civil</span>
                                <br/>
                                {this.props.data.estadocivil}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Permiso de Conducir</span>
                                <br/>
                                {this.props.data.permisoconducir}
                            </li>
                            <li className="sidebar--item sidebar--item-personal">
                                <span className="sidebar--label">Página web</span>
                                <br/>
                                {this.props.data.pagweb}
                            </li>
                        </ul>
                    </div>

                    <div className="sidebar--data">
                        <h2 className="sidebar--title" >
                            Idiomas
                        </h2>
                        <ul className="sidebar--list">
                            <li className="sidebar--item">
                                <span className="sidebar--label">{this.props.data.idiomas}</span>
                            </li>
                        </ul>
                    </div>
                </aside>
                <section className="sections">
                    <div className="section section--summary">
                        <div className="section--content">
                            <p>
                                {this.props.data.resumen}
                            </p>
                        </div>
                    </div>

                    <div className="section">
                        <div className="section--title">
                            <h2> Experiencia laboral </h2>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}