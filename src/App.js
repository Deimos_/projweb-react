import React, {Component} from 'react';
import Form from './Form';
import Preview from './Preview';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      formData: ""
    }
    this.fState = this.fState.bind(this)
  }

  fState (data) {
    this.setState({formData: data})
  }

  render() {
    return ( 
      <div>
        <Form fState={this.fState} />
        <Preview formData={this.state.formData}/>
      </div>
    );
  }
}
