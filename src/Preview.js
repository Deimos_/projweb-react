import React, { Component } from 'react'

import './index.css';
import Auckland from './Components/Auckland';
import Berkeley from './Components/Berkeley';
import Cambridge from './Components/Cambridge';
import Otago from './Components/Otago';
import Oxford from './Components/Oxford';
import Princeton from './Components/Princeton';

export default class Preview extends Component {
    constructor(props) {
        super()
        this.state = props.formData
    }

    render() {

        let s = this.props.formData
        let selected = s.template

        switch(selected){
            case "auckland":
                return (
                    <frame src="../Templates/auckland.html">
                        <div className="split right">
                            <Auckland data={this.props.formData} />
                        </div>
                    </frame>
                )
            case "berkeley":
                return (
                    <div className="split right">
                        <Berkeley data={s} />
                    </div>
                )
            case "cambridge":
                return (
                    <div className="split right">
                        <Cambridge data={s} />
                    </div>
                )
            case "otago":
                return (
                    <div className="split right">
                        <Otago data={s} />
                    </div>
                )
            case "oxford":
                return (
                    <div className="split right">
                        <Oxford data={s} />
                    </div>
                )
            case "princeton":
                return (
                    <div className="split right">
                        <Princeton data={s} />
                    </div>
                )
        }
        return (
            <div className="split right">
                <h1 className="centered">Selecciona una plantilla</h1>
            </div>
        )
    }
}
